
// card class
class Card {
    constructor(suit, number) {
        this._suit = suit;
        this._number = number;
        this._faceUp = false;
    }

    // getters
    get suit() { return this._suit; }
    get number() { return this._number; }
    get name() { return Card.NUMBER_TO_NAME(this.number); }
    get faceUp() { return this._faceUp; }

    // calculate point, ace can have 1 or 11 points
    get points() {
        if (this.number > 10) {
            return 10;
        } else if (this.number === 1) {
            return [1, 11];
        }
        return this.number;
    }

    // shows the card, turns it over
    show() { this._faceUp = true; }

    // shows the back of the card
    hide() { this._faceUp = false; }

    // a clean string representation of the card
    toString() { return `${this.name} of ${this.suit}`; }


    // static card properties
    static get SUITS() {
        return ['spades', 'hearts', 'clubs', 'diamonds'];
    }
    static get CARDS_PER_SUIT() {
        return 13;
    }

    // convertes the numeric value of a card to a name for the picture cards
    static NUMBER_TO_NAME(cardNum) {
        switch (cardNum) {
            case 1: return 'ace';
            case 11: return 'jack';
            case 12: return 'queen';
            case 13: return 'king';
            default: return cardNum;
        }
    }
};

// a deck of cards
class Deck {
    constructor(blackjack) {
        this._bj = blackjack;
        this._cards = [];

        this.fillCards();
        // this.toLog(); // log deck of cards to console
    }

    // getters
    get cards() { return this._cards; }
    get cardsRemaining() { return this._cards.length; }

    // deals a card from the deck, has face down by default
    // returns null when there are no cards remaining
    getCard(faceUp, random = true) {
        let card = null;
        const cardsRem = this.cardsRemaining;
        if (cardsRem > 0) {
            // random = false; // for debuggin purposes don't use a random card
            const cardIx = random ? Math.floor(Math.random() * cardsRem) : 0;

            // discard one card from the deck
            card = this._cards.splice(cardIx, 1)[0];

            // turn card over if needed
            if (faceUp) {
                card.show();
            }
        } else {
            console.log('All cards have been dealt already.');
        }
        return card;
    }

    fillCards() {
        Card.SUITS.forEach(suit => {
            for (let cardNum = 1; cardNum <= Card.CARDS_PER_SUIT; cardNum++) {
                this._cards.push(new Card(suit, cardNum));
            }
        });
    }

    toLog() {
        this._cards.forEach(card => {
            console.log(card.toString());
        });
    }

    // put cards back in the deck
    returnHand(hand) {
        hand.forEach(card => {
            card.hide();
            this._cards.push(card);
        });
    }
}

class Player {
    constructor(name, blackjack) {
        this._name = name;
        this._hand = [];
        this._bj = blackjack;
    }

    // getters and setters
    get name() { return this._name; }
    get hand() { return this._hand; }
    set status(status) { this._status = status; }

    // calculates score of the hand
    get score() {
        let pts = 0;
        let aces = [];

        // take the sum of all non-ace cards
        this.hand.forEach(card => {
            if (card.faceUp) {
                if (card.name === 'ace') {
                    aces.push(card);
                } else {
                    pts += card.points;
                }
            }
        });

        // variable points for aces
        aces.forEach((ace, i, aces) => {
            // last ace can be either 1 or 11 points
            // if (Object.is(aces.length - 1, i)) {
            if (aces.length - 1=== i) {
                // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax
                const min = pts + Math.min(...ace.points); // 1 pt
                const max = pts + Math.max(...ace.points); // 11 pts

                // use max score, as long as it's not over the game limit
                pts = max <= Blackjack.LIMIT ? max : min;
            }
            // there will be more aces, so always use 1 point (two 11 point aces = 22 = bust)
            else {
                pts += ace.points[0];
            }
        });

        return pts;
    }

    // returns the player status
    get status() {
        // fixed status
        if (this._status) {
            return this._status;
        }

        // score based status
        if (this.score > Blackjack.LIMIT) {
            return Status.BUST;
        } else if (this.score === Blackjack.LIMIT) {
            return Status.BLACKJACK;
        }
        // WAITING (dealer)
        else if (this.isDealer()) {
            return Status.WAITING
        }
        // CONTINUE (player)
        return Status.CONTINUE;
    }

    // is this the dealer? NO
    isDealer() {
        return false;
    }

    // receive a card from the deck
    dealCard(card) {
        this._hand.push(card);
    }

    // hit action
    hit() {
        this.dealCard(this._bj.getCard());
        this._bj.checkGameStatus(this);
    }

    // stand action
    stand() {
        this.status = Status.STAND;
        this._bj.checkGameStatus(this);
    }

    // resets the player status and returns the hand the player was holding
    reset() {
        // unset fixed status
        delete this._status;

        // empty hand
        return this._hand.splice(0);
    }
}

class Dealer extends Player {
    constructor(blackjack) {
        super('Dealer', blackjack);
    }

    // is this the dealer? YES
    isDealer() {
        return true;
    }

    // turn over all cards
    showCards() {
        this.hand.forEach(card => card.show());
    }

    // do numerous checks to determine if dealer should hit
    shouldHit() {
        const plr = this._bj.player;

        // player bust
        if (plr.status === Status.BUST) return false

        // outscoring player
        if (this.score > plr.score) return false

        // 17 or higher
        if (this.score > Dealer.STAND_AT_OR_ABOVE) return false

        // both at 16 points
        if (this.score === player.score && this.score === Dealer.STAND_AT_OR_ABOVE) return false;

        return true;
    }

    // dealer plays
    play() {
        // show cards
        this.showCards();

        // start hitting
        while (this.shouldHit()) {
            this.hit();
        }

        // stand if dealer could continue in theory
        if (this.status === Status.WAITING) { // was continue
            this.stand();
        }
        // dealer has hit blackjack or is bust
        else {
            this._bj.checkGameStatus(this);
        }
    }

    // the the value at which the dealer stands
    static get STAND_AT_OR_ABOVE() { return 16; }
}

// object with the various player and game status
class Status {
    // player status
    static get CONTINUE() { return 'continue'; }
    static get WAITING() { return 'waiting'; }
    static get STAND() { return 'stand'; }
    static get BLACKJACK() { return 'blackjack! :)'; }
    static get BUST() { return 'bust :('; }
    static get WINNER() { return 'winner :)'; }
    static get LOSER() { return 'lost :('; }
    static get TIE() { return 'tied :|'; }

    // game status
    static get GAME_START() { return `Let's deal some hands.`; }
    static get GAME_PLAYER() { return `It's the player's turn.`; }
    static get GAME_DEALER() { return `It'the dealer's turn.`; }
    static get GAME_END() { return `The game has finished.`; }
}

// blackjack game class
class Blackjack {
    constructor() {
        this._status = Status.GAME_START;

        this._deck = new Deck();
        this._ui = new UserInterface(this);
        this._dealer = new Dealer(this);
        let playerName = 'player';
        playerName = prompt(`Welcome ${playerName}, what is your name?`, playerName) || playerName;
        this._player = new Player(playerName, this);

        this.render();
    }

    // getters
    get dealer() { return this._dealer; }
    get player() { return this._player; }
    get ui() { return this._ui; }
    get status() { return this._status; }

    // dispense a card from the deck
    getCard(faceUp = true) {
        return this._deck.getCard(faceUp);
    }

    // resets the game before dealing a new hand
    resetGame() {
        // return hands back to the deck
        this._deck.returnHand(this.dealer.reset());
        this._deck.returnHand(this.player.reset());

        this._status = Status.GAME_START;

        this.render();
    }

    // deal hands to dealer & player
    deal() {
        // if a hand has been played before, reset the game
        if (this.status === Status.GAME_END) {
            this.resetGame();
        }

        // deal cards to dealer
        this.dealer.dealCard(this.getCard());
        this.dealer.dealCard(this.getCard(false));

        // deal cards to player
        this.player.dealCard(this.getCard());
        this.player.dealCard(this.getCard());

        // it's the players turn
        this._status = Status.GAME_PLAYER;

        // player might have hit blackjack
        this.checkGameStatus(this.player);
    }

    // keep track of player moves
    checkGameStatus(plr) {
        switch (plr.status) {
            case Status.BLACKJACK:
            case Status.BUST:
                this.determineWinner();
                break;
            case Status.STAND: // if player stands let dealer play, otherwise game has ended
                // dealer
                if (plr.isDealer()) {
                    this.determineWinner();
                }
                // player
                else {
                    this._status = Status.GAME_DEALER;
                    this.dealer.play();
                }
                break;
        }

        // update UI
        this.render();
    }

    // check if player is bust or broke and a winner can be determined that way
    checkBustOrBj(plr, opponent) {
        let bustOrBroke = true;
        switch (plr.status) {
            case Status.BUST:
                // plr.status = Status.LOSER;
                opponent.status = Status.WINNER;
                break;
            case Status.BLACKJACK:
                // plr.status = Status.WINNER;
                opponent.status = Status.LOSER;
                break;
            default:
                bustOrBroke = false;
        }
        return bustOrBroke;
    }

    // determines who the winner is
    determineWinner() {
        const plr = this.player;
        const dlr = this.dealer;

        // see if someone went bust or hit blackjack
        const foundWinner = this.checkBustOrBj(plr, dlr) || this.checkBustOrBj(dlr, plr);

        // nobody went bust or hit blackjack, so compare scores
        if (!foundWinner) {
            // tie
            if (plr.score === dlr.score) {
                plr.status = Status.TIE;
                dlr.status = Status.TIE;
            }
            // player wins
            else if (plr.score > dlr.score) {
                plr.status = Status.WINNER;
                dlr.status = Status.LOSER;
            }
            // dealer wins
            else {
                plr.status = Status.LOSER;
                dlr.status = Status.WINNER;
            }
        }

        // update game status
        this._status = Status.GAME_END;

        // update UI
        this.render();
    }

    // render UI with the game status
    render() {
        this.ui.render(this._status);
    }

    // static properties
    static get LIMIT() { return 21; }
}

// object handling the user interface
class UserInterface {
    constructor(blackjack) {
        this._bj = blackjack;
        this._buttons = {
            deal: document.getElementById('deal'),
            hit: document.getElementById('hit'),
            stand: document.getElementById('stand')
        };

        this.generateCardStyles();
        this.addButtonHandlers();
    }

    // add dynamic styles for the card background images (too lazy to type them all)
    generateCardStyles() {
        const styleEl = document.getElementById('cardStyles');
        // loop through a fresh deck of cards and generate css styling for card background images
        const deck = new Deck();
        deck.cards.forEach(card => {
            styleEl.innerHTML += `.${card.suit}.c${card.number} { background-color: inherit; background-image: url("images/cards/${card.name}_of_${card.suit}${card.number > 10 ? 2 : ''}.svg"); }\n`;
        });
    }

    // handle button clicks
    addButtonHandlers() {
        var bj = this._bj;
        for (const [btnId, btn] of Object.entries(this._buttons)) {
            btn.onclick = (event) => {
                switch (btnId) {
                    case 'deal':
                        bj.deal();
                        break;
                    case 'hit':
                        bj.player.hit();
                        break;
                    case 'stand':
                        bj.player.stand();
                        break;
                    default:
                        console.log('unknown button detected', btn);
                        break;
                }
            };
        };
    }

    // render buttons depending on the game status
    renderButtons(gameStatus) {
        const deal = this._buttons.deal;
        const hit = this._buttons.hit;
        const stand = this._buttons.stand;

        // deal
        switch (gameStatus) {
            case Status.GAME_START:
            case Status.GAME_END:
                deal.removeAttribute('style');
                break;
            default:
                deal.style.display = 'none';
                break;
        }

        // hit
        switch (gameStatus) {
            case Status.GAME_START:
            case Status.GAME_END:
                hit.style.display = 'none';
                hit.toggleAttribute('disabled', true);
                break;
            case Status.GAME_PLAYER:
                hit.removeAttribute('style');
                hit.toggleAttribute('disabled', false);
                break;
            default:
                hit.toggleAttribute('disabled', true);
                break;
        }

        // stand
        switch (gameStatus) {
            case Status.GAME_PLAYER:
                stand.toggleAttribute('disabled', false);
                break;
            default:
                stand.toggleAttribute('disabled', true);
                break;
        }

    }

    // render player info on the table
    renderPlayer(playerId, player) {
        const tableSection = document.getElementById(playerId);
        const scoreUI = tableSection.getElementsByClassName('score')[0];
        const cardsUI = tableSection.getElementsByClassName('cards')[0];

        // render the score
        scoreUI.getElementsByClassName('name')[0].innerText = player.name;
        scoreUI.getElementsByClassName('points')[0].innerText = player.score;
        scoreUI.getElementsByClassName('status')[0].innerText = player.status;

        // clear the cards currently on display
        cardsUI.innerHTML = '';

        // render the cards
        player.hand.forEach(card => {
            cardsUI.appendChild(this.renderCard(card));
        });
    }

    // create a card element to be added to the UI
    renderCard(card) {
        const el = document.createElement('div');
        el.classList.toggle('card', true);

        // show front of the card
        if (card.faceUp) { // TODO: remove
            el.classList.toggle(card.suit, true);
            el.classList.toggle(`c${card.number}`, true);
        }
        // card is face down so just show background
        else {
            el.classList.toggle('facedown', true);
        }

        return el;
    }

    // render table based on gameStatus
    render(gameStatus) {
        if (!gameStatus) {
            console.log('Missing gamestatus for UI render!');
        }

        this.renderPlayer('dealer', this._bj.dealer);
        this.renderPlayer('player', this._bj.player);

        this.renderButtons(gameStatus);
    }
};

new Blackjack();